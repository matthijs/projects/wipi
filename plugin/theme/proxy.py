# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - Generic website theme.

    @copyright: 2009+ Matthijs Kooijman
    @license: GNU GPL, see COPYING for details.

    This theme simply loads other themes, depending on the url requested.

    To use this theme, set the default theme to 'proxy' and set the
    configuration value 'proxy_theme_mapping' to a list of (prefix, themename)
    tuples (which will be tested in order, so be sure to include a theme with
    an empty prefix at the end). For example:

        proxy_theme_mapping = [
            ('/Site/', 'deadfoxjunction'),
            ('', 'modernized'),
        ]

    The prefixes are matched against the page requested, so urls within
    MoinMoin (and a trailing slash is included).

    This theme does not currently play well with user-selectable themes, since
    that would completely bypass the proxy plugin. For now it is probably best
    to set the theme_force configuration variable.
"""

from MoinMoin.theme import load_theme, ThemeNotFound

def Theme(context):
    """
    Usually, a theme plugin defines a Theme class, whose constructor is
    called.  Instead, we define a Theme function that calls the proper
    constructor.
    """
    path = context.request.path
    # Always add a trailing space, so that a /Foo/ also matches the /Foo
    # path (without needing to remove the trailing space from the
    # prefix, which would make also match /FooBar).
    if path[-1] != '/':
        path += '/'
    for (prefix, theme) in context.cfg.proxy_theme_mapping:
        if path.startswith(prefix):
            return load_theme(context, theme)

    raise ThemeNotFound("proxy_theme_mapping defines no theme for url: %s" % (context.request.path))

# vim: set sw=4 sts=4 expandtab:
