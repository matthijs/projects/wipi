# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - Generic website theme.

    @copyright: 2009+ Matthijs Kooijman
    @license: GNU GPL, see COPYING for details.

    This theme is meant for wiki's that are meant to function as a website,
    meaning nothing fancy and wiki-ish (at least when you're not logged in).

    This theme is meant to be subclassed. Subclasses should at least define a
    name, and possibly override other stuff.
"""

from MoinMoin.theme import ThemeBase
from MoinMoin.Page import Page
from MoinMoin import wikiutil
from StringIO import StringIO

class SiteTheme(ThemeBase):

    def editbar(self, d):
        if self.request.user.valid:
            return ThemeBase.editbar(self, d)
        else:
            return ''

    def searchform(self, d):
        if self.request.user.valid:
            return ThemeBase.searchform(self, d)
        else:
            return ''
    
    def navibar(self, d):
        if self.request.user.valid:
            return ThemeBase.navibar(self, d)
        else:
            return ''

    def header(self, d, **kw):
        """ Assemble wiki header
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<div id="header">',
            self.logo(),
            self.menu(d),
            #self.username(d),
            #u'<div id="locationline">',
            #self.interwiki(d),
            #self.title(d),
            #u'</div>',
            #self.navibar(d),
            #u'<hr id="pageline">',
            #u'<div id="pageline"><hr style="display:none;"></div>',
            ##self.trail(d),
            u'</div>',

            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),

            self.msg(d),
            self.editbar(d),

            # Start of page
            self.startPage(d),
        ]
        return u'\n'.join(html)

    def editorheader(self, d, **kw):
        """ Assemble wiki header for editor
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<div id="header">',
            self.logo(),
            self.menu(d),
            u'</div>',

            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),

            # Start of page
            self.startPage(d),
            self.msg(d),
        ]
        return u'\n'.join(html)

    def footer(self, d, **keywords):
        """ Assemble wiki footer
        
        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """
        page = d['page']
        html = [
            # End of page
            self.endPage(d),

            # Pre footer custom html (not recommended!)
            self.emit_custom_html(self.cfg.page_footer1),

            # Footer
            u'<div id="footer">',
            self.username(d),
            self.pageinfo(page),
            self.searchform(d),
            #self.editbar(d),
            #self.credits(d),
            #self.showversion(d, **keywords),
            u'<div id="footerbottom"></div>',
            u'</div>',

            # Post footer custom html
            self.emit_custom_html(self.cfg.page_footer2),
            ]
        return u'\n'.join(html)

    def menu(self, d):
        """ Assemble a "main" menu
            
            @param d: parameter dictionary
            @rtype:   unicode
            @return: menu html
        """
        menu = Page(self.request, 'Site/Menu')
        return u'<div id="menubar">%s</div>' % parse_wiki_page(self.request, menu)

    def theme_script(self, name):
        """ Format script html from this theme's static dir """
        src = '%s/%s/js/%s.js' % (self.request.cfg.url_prefix_static, self.name, name)
        return '<script type="text/javascript" src="%s"></script>' % src

    def pageinfo(self, page):
        """ Output page name and "last modified". This overwrites the pageinfo
        from ThemeBase to hide the username, which is useless and always
        produces a link to the non-existing homepage of the last editor. """
        _ = self.request.getText
        html = ''
        if self.shouldShowPageinfo(page):
            info = page.lastEditInfo()
            if info:
                info = _("last modified %(time)s") % info
                pagename = page.page_name
                info = "%s  (%s)" % (wikiutil.escape(pagename), info)
                html = '<p id="pageinfo" class="info"%(lang)s>%(info)s</p>\n' % {
                    'lang': self.ui_lang_attr(),
                    'info': info
                    }
        return html

    def startPage(self, d):
        # This opens up #page
        html = ThemeBase.startPage(self)
        html += u'<div id="pagetop"></div>\n'
        html += u'<div id="pagemiddle">\n'
        return html

    def endPage(self, d):
        html = u'</div><!-- #pagemiddle -->\n'
        html += ThemeBase.endPage(self)
        # This adds #pagebottom and closes #page
        return html

def parse_wiki_page(request, page):
    """
    This is an ugly hack to render a page into a string. By default,
    formatters render using request.write automatically, which prevents us
    from capturing the output. By disguising a StringIO buffer as a request
    object, we manage to get at the rendered contents.

    However, when {{{#!wiki or similar blocks are used, stuff breaks (since
    that creates a second parser that doesn't get our StringIO buffer).
    """
    Parser = wikiutil.searchAndImportPlugin(request.cfg, "parser", 'wiki')
    # Create a stringIO buffer, to capture the output
    buffer = StringIO()
    # Make the buffer look like the request, since the parser writes
    # directly to the request
    buffer.form = request.form
    buffer.getText = request.getText
    buffer.cfg = request.cfg
    # Create a new formatter. Since we need to set its page, we can't use
    # request.formatter.
    from MoinMoin.formatter.text_html import Formatter
    formatter = Formatter(request)
    formatter.setPage(page)

    # Create the parser and parse the page
    parser = Parser(page.data, buffer)
    parser.format(formatter)
    # Return the captured buffer
    return buffer.getvalue()

# vim: set sw=4 sts=4 expandtab:
