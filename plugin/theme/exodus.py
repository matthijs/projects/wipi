"""
    MoinMoin - Exodus website theme.

    @copyright: 2009+ Matthijs Kooijman
    @license: GNU GPL, see COPYING for details.
"""

from site import SiteTheme
from MoinMoin.theme import ThemeBase

class Theme(SiteTheme):

    name = "exodus"

    def html_head(self, d):
        html = [
            SiteTheme.html_head(self, d),
            #self.theme_script('jquery-1.3.2.min'),
            self.theme_script('jquery-1.3.2'),
        ]
        if d['page'].page_name == 'Site':
            html.append(self.theme_script('homepage'))
            html.append(self._stylesheet_link(True, 'screen', 'home'))

        return '\n'.join(html)

    def startPage(self, d):
        # For the homepage, don't use SiteTheme's changes
        if d['page'].page_name == 'Site':
            return ThemeBase.startPage(self)
        else:
            return SiteTheme.startPage(self, d)

    def endPage(self, d):
        # For the homepage, don't use SiteTheme's changes, but also don't use
        # ThemeBase, since that adds a #pagebottom.
        if d['page'].page_name == 'Site':
            return u'</div><!-- #page -->'
        else:
            return SiteTheme.endPage(self, d)


def execute(request):
    """
    Generate and return a theme object
        
    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)

# vim: set sw=4 sts=4 expandtab:
