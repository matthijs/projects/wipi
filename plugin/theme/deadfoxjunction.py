"""
    MoinMoin - Dead fox junction website theme.

    @copyright: 2010+ Matthijs Kooijman
    @license: GNU GPL, see COPYING for details.
"""

from site import SiteTheme

class Theme(SiteTheme):
    name = "deadfoxjunction"

    # Add an extra #window div that will be visible content area, framed
    # by a wooden "window". We can't just use #content here, since
    # #messages is besides #content in editing mode.
    def startPage(self, d):
    	html = SiteTheme.startPage(self, d)
	html += u'<div id="window">\n'
	return html

    def endPage(self, d):
    	html = SiteTheme.endPage(self, d)
	html += u'</div><!-- #window -->\n'
	return html

    def recentchanges_entry(self, d):
        """
        Assemble a single recentchanges entry (table row)

        Based on the same function from ThemeBase, but with the comment
        on a second table row, since the available horizontal space in
        this theme is fairly limited.

        @param d: parameter dictionary
        @rtype: string
        @return: recentchanges entry html
        """
        _ = self.request.getText
        html = []
        html.append('<tr>\n')

        html.append('<td class="rcicon1">%(icon_html)s</td>\n' % d)

        html.append('<td class="rcpagelink">%(pagelink_html)s</td>\n' % d)

        html.append('<td class="rctime">')
        if d['time_html']:
            html.append("%(time_html)s" % d)
        html.append('</td>\n')

        html.append('<td class="rcicon2">%(info_html)s</td>\n' % d)

        html.append('<td class="rceditor">')
        if d['editors']:
            html.append('<br>'.join(d['editors']))
        html.append('</td>\n')
        html.append('</tr>\n')
        html.append('<tr>\n')

        html.append('<td colspan="5" class="rccomment">')
        if d['comments']:
            if d['changecount'] > 1:
                notfirst = 0
                for comment in d['comments']:
                    html.append('%s<tt>#%02d</tt>&nbsp;%s' % (
                        notfirst and '<br>' or '', comment[0], comment[1]))
                    notfirst = 1
            else:
                comment = d['comments'][0]
                html.append('%s' % comment[1])
        html.append('</td>\n')

        html.append('</tr>\n')

        return ''.join(html)
