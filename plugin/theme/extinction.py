# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - Extinction website theme.

    @copyright: 2009+ Matthijs Kooijman
    @license: GNU GPL, see COPYING for details.
"""

from site import SiteTheme

class Theme(SiteTheme):
    name = "extinction"

