#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - FastCGI Driver Script
    
    @copyright: 2008 MoinMoin:ThomasWaldmann
    @license: GNU GPL, see COPYING for details.
"""

import sys, os

# Define the path to our config relative to where we are.
confdir = os.path.join(os.path.dirname(__file__), 'conf')

# Put our config in the python path
sys.path.insert(0, confdir)

# Load a logging conf
from MoinMoin import log
log.load_config(os.path.join(confdir, 'logging.conf'))

from MoinMoin.web.flup_frontend import CGIFrontEnd

options = [
            # Show errors in the output
            #"--debug", "web",
          ]
CGIFrontEnd().run(options)
