var active = false;

//changes the preview image
function switchCloseup(to) {
	/* Deselect all closeups */
	$(".closeup div").removeClass('selected');
	/* Select the wanted one */
	$(".closeup div." + to).addClass('selected');
}

//hides all previewcontent and reveals the selected info
function switchInfo(cls) {
	/* Deselect all summaries */
	$(".summary div").removeClass('selected');
	/* Select the wanted one */
	$(".summary div." + cls).addClass('selected');
	/* Save the selected area to reset to on mouseout */
	active = cls;
}

//after loading the document do this:
$(document).ready( function () {
	$("area").mouseout (function (){
		/* Switch back to the selected area */
		switchCloseup(active);
	});

	$("area").mousemove (function (){
		/* Switch to the area we're hovering over */
		switchCloseup($(this).attr('class'));
	});

	$("area").click (function (){
		/* Select a different area */
		switchInfo($(this).attr('class'));
		return false;
	});
});
