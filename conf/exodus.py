# -*- coding: utf-8 -*-
# IMPORTANT! This encoding (charset) setting MUST be correct! 

# we import the FarmConfig class for common defaults of our wikis:
from farmconfig import FarmConfig

from farmconfig import data_dir_root

# now we subclass that config (inherit from it) and change what's different:
class Config(FarmConfig):

    # basic options
    sitename = u'Exodus' # [Unicode]
    interwikiname = 'Exodus'

    data_dir = data_dir_root + '/exodus/'

    # The GUI WYSISYG editor is not installed with Debian.
    # See /usr/share/doc/$(cdbs_curpkg)/README.Debian for more info
    editor_force = True
    editor_default = 'text'  # internal default, just for completeness

    # Use a site-theme for /Site/ and a normal wiki theme for the rest.
    proxy_theme_mapping = [
        ('/Site/', 'exodus'),
        ('', 'modernized'),
    ]
     
    # Set cookies on /, so they work on /wipi and /Site alike.
    cookie_path = '/'

# vim: set ts=8 sts=4 sw=4 expandtab:
