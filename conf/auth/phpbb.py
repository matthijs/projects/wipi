# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - auth plugin doing a check against MySQL db

    @copyright: 2008 Matthijs Kooijman
    @license: GNU GPL, see COPYING for details.

    This plugin allows authentication (use accounts and password) and
    authorization (use groups) against a phpbb Mysql database.

    To use this plugin, you should put it in a place where the config python
    file can "see" it (somewhere in your pythonpath, or in the same dir as the
    config file). Then import the setup function and call it.

    For example:

    class FarmConfig:
        import phpbb
        (phpbb_auth, phpbb_groups) = phpbb.setup(...)
        auth = [phpbb_auth]
        groups = phpbb_groups
"""

import MySQLdb
# Password encryption module. Python port of the method used by phpbb3.
import phpass
from MoinMoin import user
from MoinMoin.auth import BaseAuth, ContinueLogin
from MoinMoin.datastruct.backends import LazyGroupsBackend, LazyGroup
from MoinMoin import log
logging = log.getLogger(__name__)


def connect(dbhost=None, dbport=None, dbname=None, dbuser=None, dbpass=None, **kwargs):
    # This code was shamelessly stolen from
    # django.db.backends.mysql.base.cursor
    kwargs = {
        'charset': 'utf8',
        'use_unicode': False,
    }
    if dbuser:
        kwargs['user'] = dbuser
    if dbname:
        kwargs['db'] = dbname
    if dbpass:
        kwargs['passwd'] = dbpass
    if dbhost.startswith('/'):
        kwargs['unix_socket'] = dbhost
    elif dbhost:
        kwargs['host'] = dbhost
    if dbport:
        kwargs['port'] = int(dbport)

    # End stolen code

    try:
        conn = MySQLdb.connect (**kwargs)
    except:
        import sys
        import traceback
        info = sys.exc_info()
        logging.error("phpbb: authentication failed due to exception connecting to DB, traceback follows...")
        logging.error(''.join(traceback.format_exception(*info)))
        return False

    return conn


class PhpbbGroupsBackend(LazyGroupsBackend):
    class PhpbbGroup(LazyGroup):
        """
        A group from phpbb.
        """
        pass

    def __init__(self, request, **kwargs):
        super(LazyGroupsBackend, self).__init__(request)

        self.request = request
        self.dbconfig = kwargs

    def __iter__(self):
        """
        Return a list of group names.
        """
        return self.list_query("SELECT group_name \
                                FROM `%sgroups`"
                                % self.dbconfig['phpbb_prefix'])

    def __contains__(self, group_name):
        """
        Does a group with the given name exist?
        """
        return self.single_query("SELECT EXISTS ( \
                                      SELECT * \
                                      FROM `%sgroups` \
                                      WHERE group_name=%%s)" % self.dbconfig['phpbb_prefix'],
                                 group_name)

    def __getitem__(self, group_name):
        """
        Get the group with the given name.
        """
        return self.PhpbbGroup(self.request, group_name, self)

    def _iter_group_members(self, group_name):
        """
        Get all member names for the given group. This is called by
        LazyGroup.__iter__.
        """
        return self.list_query ("SELECT username \
                                 FROM `%susers` as u, `%suser_group` as ug, `%sgroups` as g  \
                                 WHERE u.user_id = ug.user_id AND ug.group_id = g.group_id \
                                       AND ug.user_pending = 0 AND g.group_name = %%s"
                                 % (self.dbconfig['phpbb_prefix'], self.dbconfig['phpbb_prefix'], self.dbconfig['phpbb_prefix']),
                                group_name)

    def _group_has_member(self, group_name, member):
        """
        Does the group with the given name have a member with the given name?
        This is called by LazyGroup.__contains__.
        """
        return self.single_query ("SELECT EXISTS( \
                                       SELECT * \
                                       FROM `%susers` as u, `%suser_group` as ug, `%sgroups` as g \
                                       WHERE u.user_id = ug.user_id AND ug.group_id = g.group_id \
                                             AND ug.user_pending = 0 \
                                             AND g.group_name = %%s AND u.username = %%s)"
                                   % (self.dbconfig['phpbb_prefix'], self.dbconfig['phpbb_prefix'], self.dbconfig['phpbb_prefix']),
                                  (group_name, member))
        
    def groups_with_member(self, member):
        """
        Return the group names for all groups that have a member with the
        given name.
        """
        return self.list_query ("SELECT g.group_name \
                                 FROM `%susers` as u, `%suser_group` as ug, `%sgroups` as g \
                                 WHERE u.user_id = ug.user_id AND ug.group_id = g.group_id \
                                       AND ug.user_pending = 0 AND u.username = %%s"
                                % (self.dbconfig['phpbb_prefix'], self.dbconfig['phpbb_prefix'], self.dbconfig['phpbb_prefix']),
                                member)

    def single_query(self, *args):
        """
        Runs an SQL query, that returns single row with a single column.
        Returns just that single result.
        """
        conn = None
        cursor = None
        try:
            conn = connect(**self.dbconfig)
            cursor = conn.cursor()
            cursor.execute(*args)

            return cursor.fetchone()[0]
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()
        
    def list_query(self, *args):
        """
        Runs an SQL query, that returns any number of single-column rows.
        Returns the results as a list of single values
        """
        conn = None
        cursor = None
        try:
            conn = connect(**self.dbconfig)
            cursor = conn.cursor()
            cursor.execute(*args)

            for row in cursor:
                yield row[0]
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()
        
class PhpbbAuth(BaseAuth):
    logout_possible = True
    login_inputs    = ['username', 'password']
    
    def __init__(self, name='phpbb', hint=None, **kwargs):
        """
            Authenticate using credentials from a phpbb database

            The name parameter should be unique among all authentication methods.

            The hint parameter is a snippet of HTML that is displayed below the login form.
        """
        self.dbconfig = kwargs
        self.name    = name
        self.hint    = hint
        self.hash    = phpass.PasswordHash()

    def check_login(self, request, username, password):
        """ Checks the given username password combination. Returns the
        real username and corresponding emailaddress, or (False, False)
        if authentication failed. Username checks are case insensitive,
        so the real username (with the real casing) is returned (since
        ACL checks _are_ case sensitive).
        """
        conn = connect(**self.dbconfig)

        if not conn:
            return (False, False)

        # Get some data. Note that we interpolate the prefix ourselves, since
        # letting the mysql library do it only works with values (it adds ''
        # automatically). Note also that this allows possible SQL injection
        # through the phpbb_prefix variable, but that should be a trusted
        # value anyway.
        # Finally note that by default, the phpbb database specifies a
        # case insensitive collaction for the username field, so
        # usernames are checked in case insensitive manner.
        cursor = conn.cursor ()
        cursor.execute ("SELECT user_password,user_email,username FROM `%susers` WHERE LOWER(username)=LOWER(%%s)" % self.dbconfig['phpbb_prefix'], username)

        # No data? No login.
        if (cursor.rowcount == 0):
            conn.close()
            return (False, False)
       
        # Check password
        row = cursor.fetchone()
        conn.close()

        if self.hash.check_password(password, row[0]):
            return (row[1], row[2])
        else:
            return (False, False)

    def login(self, request, user_obj, **kw):
        """
        Handle a login. Called by moinmoin.
        """
        try:
            username = kw.get('username')
            password = kw.get('password')

            logging.debug("phpbb_login: Trying to log in, username=%r " % (username))
           
            # simply continue if something else already logged in
            # successfully
            if user_obj and user_obj.valid:
                return ContinueLogin(user_obj)

            # Deny empty username or passwords
            if not username or not password:
                return ContinueLogin(user_obj)

            (email, real_username) = self.check_login(request, username, password)
            
            # Login incorrect
            if (not email):
                logging.debug("phpbb_login: authentication failed for %s" % (username))
                return ContinueLogin(user_obj)

            logging.debug("phpbb_login: authenticated %s (email %s, real username %s)" % (username, email, real_username))

            # We use the username from the database (real_username)
            # here, since the username from the request might have
            # "wrong" casing (and ACL checks are case sensitive).
            u = user.User(request, auth_username=real_username, auth_method=self.name, auth_attribs=('name', 'password', 'email'))
            u.email = email
            #u.remember_me = 0 # 0 enforces cookie_lifetime config param
            u.create_or_update(True)

            return ContinueLogin(u)
        except:
            import sys
            import traceback
            info = sys.exc_info()
            logging.error("phpbb_login: authentication failed due to unexpected exception, traceback follows...")
            logging.error(''.join(traceback.format_exception(*info)))
            return ContinueLogin(user_obj)

    def login_hint(self, request):
        """ Return a snippet of HTML that is displayed with the login form. """
        return self.hint

def setup(**kwargs):
    """
    Setup the phpbb backend. Takes the following keyword arguments:
    dbhost -- The database server host
    dbport -- The database server portname
    dbname -- The database name
    dbuser -- The username to log in
    dbpass -- The password to log in
    phpbb_prefix -- The table name prefix used for this phpbb installation
    name -- The name to use for the auth backend
    hint -- A hint to show in the login interface (HTML string) 

    This function can be called multiple times to create backends for
    different phpbb installations

    Returns a tuple (auth, groups) containing an (instantiated) auth backend
    and a groups backend (constructor). These can be put directly into the
    "auth" (as part of the list) and "groups" (directly) config directives.

    e.g.,
    
    class FarmConfig:
        (phpbb_auth, phpbb_groups) = phpbb.setup(...)
        auth = [phpbb_auth]
        groups = phpbb_groups

    (actual configuration parameters to setup() are omitted in this example)
    """
    
    # Create a "constructor" to create a phpbb_groups instance, while
    # passing ourselves to it.
    groups = lambda config, request: PhpbbGroupsBackend(request, **kwargs)
    # Create an instantiated auth backend.
    auth = PhpbbAuth(**kwargs)

    return (auth, groups)
    
# vim: set sw=4 expandtab sts=4:vim
