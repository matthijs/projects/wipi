# -*- coding: utf-8 -*-
# IMPORTANT! This encoding (charset) setting MUST be correct!

"""
    MoinMoin - Configuration for a wiki farm

    Note that there are more config options than you'll find in
    the version of this file that is installed by default; see
    the module MoinMoin.config.multiconfig for a full list of names and their
    default values.

    Also, the URL http://moinmo.in/HelpOnConfiguration has
    a list of config options.
"""


# Wikis in your farm --------------------------------------------------

# If you run multiple wikis, you need this list of pairs (wikiname, url
# regular expression). moin processes that list and tries to match the
# regular expression against the URL of this request - until it matches.
# Then it loads the <wikiname>.py config for handling that request.

# Important:
#  * the left part is the wikiname enclosed in double quotes
#  * the left part must be a valid python module name, so better use only
#    lower letters "a-z" and "_". Do not use blanks or "-" there!!!
#  * the right part is the url re, use r"..." for it
#  * the right part does NOT include "http://" nor "https://" at the beginning
#  * in the right part ".*" means "everything". Just "*" does not work like
#    for filenames on the shell / commandline, you must use ".*" as it is a RE.
#  * in the right part, "^" means "beginning" and "$" means "end"

wikis = [
    # Standalone server needs the port e.g. localhost:8000
    # Twisted server can now use the port, too.

    # wikiname,     url regular expression (no protocol)
    # ---------------------------------------------------------------
    ("extinction", r"http://extinction.evolution-events.nl/wipi"),
    ("exodus", r"http://exodus.evolution-events.nl/wipi"),
    ("deadfoxjunction", r"http://deadfoxjunction.evolution-events.nl/wipi"),
    # Pretend we're also available at /. In reality, only the /Site pages are
    # available there due to lighttpd rewriting. Due to proper setting of
    # script-name by lighttpd and the url_mappings setting below all links
    # still work too!
    ("exodus", r"http://exodus.evolution-events.nl"),
    ("deadfoxjunction", r"http://deadfoxjunction.evolution-events.nl"),
]


# Common configuration for all wikis ----------------------------------

# Everything that should be configured the same way should go here,
# anything else that should be different should go to the single wiki's
# config.
# In that single wiki's config, we will use the class FarmConfig we define
# below as the base config settings and only override what's different.
#
# In exactly the same way, we first include MoinMoin's Config Defaults here -
# this is to get everything to sane defaults, so we need to change only what
# we like to have different:

from MoinMoin.config.multiconfig import DefaultConfig

# Now we subclass this DefaultConfig. This means that we inherit every setting
# from the DefaultConfig, except those we explicitely define different.

# Local setting, defined by matthijs for easy moving of data. It didn't work
# to define this in the FarmConfig class, so just put it here..
import os
data_dir_root = os.path.join(os.environ['SITE_DIR'], 'data', 'wipi')

class FarmConfig(DefaultConfig):

    # Critical setup  ---------------------------------------------------

    # Where read-only system and help page are. You might want to share
    # this directory between several wikis. When you update MoinMoin,
    # you can safely replace the underlay directory with a new one. This
    # directory is part of MoinMoin distribution, you don't have to
    # backup it.
    data_underlay_dir = data_dir_root + '/underlay/'

    # Override the default data_dir/plugin path for plugins, which doesn't
    # make sense. We don't want to have per-wiki plugins anyway and stuffing
    # code in the datadir is weird.
    plugin_dir = os.path.join(os.path.dirname(__file__), os.path.pardir, 'plugin')

    # The URL prefix we use to access the static stuff (img, css, js).
    # NOT touching this is maybe the best way to handle this setting as moin
    # uses a good internal default (something like '/moin_static163' for moin
    # version 1.6.3).
    # For Twisted and standalone server, the default will automatically work.
    # For others, you should make a matching server config (e.g. an Apache
    # Alias definition pointing to the directory with the static stuff).
    url_prefix_static = '/wipi/static'


    # Security ----------------------------------------------------------

    # Link spam protection for public wikis (uncomment to enable).
    # Needs a reliable internet connection.
    #from MoinMoin.security.antispam import SecurityPolicy

    acl_hierarchic = True

    # Give all rights to the EE group, but no rights to anonymous users.
    acl_rights_default = u""
    acl_rights_before = u"Evolution Events - Algemeen Bestuur:read,write,delete,revert,admin"

    # Mail --------------------------------------------------------------

    # Not configured


    # User interface ----------------------------------------------------

    # Add your wikis important pages at the end. It is not recommended to
    # remove the default links.  Leave room for user links - don't use
    # more than 6 short items.
    # You MUST use Unicode strings here, but you need not use localized
    # page names for system and help pages, those will be used automatically
    # according to the user selected language. [Unicode]
    navi_bar = [
        u'RecentChanges',
        u'FindPage',
        u'HelpContents',
    ]

    # Always use the proxy. Specific configs define a proxy_theme_mapping,
    # which maps specific paths to a corresponding theme.
    theme_default = 'proxy'
    theme_force = True

    # Language options --------------------------------------------------

    # The main wiki language, set the direction of the wiki pages
    language_default = 'en'

    # You must use Unicode strings here [Unicode]
    page_category_regex = u'^Category[A-Z]'
    page_dict_regex = u'[a-z]Dict$'
    page_group_regex = u'[a-z]Group$'
    page_template_regex = u'[a-z]Template$'

    # Content options ---------------------------------------------------

    # name of entry page
    page_front_page = u"Start"

    # Show users hostnames in RecentChanges
    show_hosts = 1

    # Show the interwiki name (and link it to page_front_page) in the Theme,
    # nice for farm setups or when your logo does not show the wiki's name.
    show_interwiki = 1
    logo_string = u''

    # The GUI WYSISYG editor is not installed with Debian.
    # See /usr/share/doc/$(cdbs_curpkg)/README.Debian for more info
    editor_force = True
    editor_default = 'text'  # internal default, just for completeness
    # Require a comment on page changes. This needs a patched moinmoin.
    require_comment = True

    # Authentication
    import auth.phpbb
    import dbsettings
    (phpbb_auth, phpbb_groups) = auth.phpbb.setup(
        name    = 'phpbb',
        dbhost  = dbsettings.phpbb_dbhost,
        dbuser  = dbsettings.phpbb_dbuser,
        dbpass  = dbsettings.phpbb_dbpass,
        dbname  = dbsettings.phpbb_dbname,
        phpbb_prefix = dbsettings.phpbb_prefix,
        hint    = "Hier kun je inloggen met je Evolution Events Forum account.  dit is vooral nuttig voor bestuursleden om wijzigingen te maken."
    )
    auth = [phpbb_auth]
    groups = phpbb_groups

    user_autocreate = True

    # All urls that would be generated below /wipi/Site can be pointed to
    # /Site instead, since lighttpd maps /Site to the wipis as well.
    url_mappings = {'/wipi/Site' : '/Site'}

    
# vim: set ts=8 sts=4 sw=4 expandtab:
